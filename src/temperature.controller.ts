import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemparatureService } from './temperature.service';

@Controller('temperature')
export class TemparatureController {
  constructor(private readonly temperatureService: TemparatureService) {}
  @Get('convert')
  convert(@Query() celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }

  @Post('convert')
  convertByPost(@Body() celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }

  @Get('convert/:celsius')
  convertParam(@Param() celsius: string) {
    return this.temperatureService.convert(parseFloat(celsius));
  }
}
