import { Injectable } from '@nestjs/common';

@Injectable()
export class TemparatureService {
  convert(celsius: number) {
    return {
      celsius: celsius,
      fahrenheit: (celsius * 9.0) / 5 + 32,
    };
  }
}
