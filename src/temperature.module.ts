import { Module } from '@nestjs/common';
import { TemparatureController } from './temperature.controller';
import { TemparatureService } from './temperature.service';

@Module({
  imports: [],
  exports: [],
  controllers: [TemparatureController],
  providers: [TemparatureService],
})
export class TemparatureModule {}
