import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemparatureController } from './temperature.controller';
import { TemparatureService } from './temperature.service';
import { UsersModule } from './users/users.module';

@Module({
  imports: [UsersModule],
  controllers: [AppController, TemparatureController],
  providers: [AppService, TemparatureService],
  exports: [],
})
export class AppModule {}
